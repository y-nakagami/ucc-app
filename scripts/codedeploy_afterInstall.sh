#!/bin/bash
set -euo pipefail
shopt -s dotglob

cd /var/www/service_tmp
service=$(cat .service)

mkdir -p /var/www/${service}
cp -r /var/www/service_tmp/* /var/www/${service}/
cd /var/www/${service}
# composer更新
php -d memory_limit=-1 /usr/local/bin/composer install -n

# laravel
sudo chmod -R 777 /var/www/${service}/storage
